<?php

$qtdTimesPorGrupo = 5;
$qtdEquipes = 80;
$qtdGrupos = $qtdEquipes / $qtdTimesPorGrupo;
$qtdVencedoresPorGrupo = 2;
$qtdMaxVencedoresGrupos = $qtdGrupos * $qtdVencedoresPorGrupo;
$maxVitorias = 16;
$minJogos = $qtdTimesPorGrupo * $qtdTimesPorGrupo;
$qtdGruposComVitoria = 0;

$grupos = [];
$equipes = [];
$jogosgrupos = [];
$equipesplayoffs = [];
$equipesplayoffs_r1 = []; //os 16
$equipesplayoffs_r2 = []; //os 8
$equipesplayoffs_r3 = []; //Semifinal
$equipesplayoffs_r4 = []; //Final

//Definição de cada equipe
for ($i = 1; $i <= $qtdEquipes; $i++) {
	$equipes[] = [
		'nome' => "Equipe #{$i}",
		'idgrupo' => -1,
		'fatorpoder' => rand(500, 1000),
		/* 'fatorpoder' define um índice para calcular se, no instante da partida,
		a equipe estava melhor ou pior para vencer (tática para desempatar) */
		'pontuacaogrupos' => 0,
		'qtdjogosgrupos' => 0,
		'indvencedorgrupos' => false
	];
}

//Definição de cada grupo
$idMaxEquipe = ($qtdEquipes - 1);
for ($i = 1; $i <= $qtdGrupos; $i++) {
	$idgrupoatu = ($i - 1);

	$grupos[] = [
		'nome' => "Grupo #{$i}",
		'equipes' => [],
		'equipesvencedoras' => [],
		'ordempontuacao' => []
	];

	for ($j = 0; $j < $qtdTimesPorGrupo; $j++) {
		do {
			$idequipe = rand(0, $idMaxEquipe);
		} while ($equipes[$idequipe]['idgrupo'] >= 0); //É preciso verificar o tipo, pois em php null == '' == "" == 0 == '0' == "0"

		$equipes[$idequipe]['idgrupo'] = $idgrupoatu;
		$grupos[$idgrupoatu]['equipes'][] = $equipes[$idequipe];
	}
}

function calcFatorDisputa($adv1, $adv2)
{
	//Definindo um fator do dividendo igual para todos da partida (simula o fator tempo)
	$fatorTempo = rand(50, 100);

	do {
		$fatorProvEq1 = rand(130, 150);
		$fatorProvEq2 = rand(130, 150);
		$novoFatorEq1 = $adv1['fatorpoder'] * ($fatorProvEq1 / $fatorTempo);
		$novoFatorEq2 = $adv2['fatorpoder'] * ($fatorProvEq2 / $fatorTempo);
	} while ($novoFatorEq1 == $novoFatorEq2); //Daqui só sai até estar com fatores desempatados

	return [$novoFatorEq1, $novoFatorEq2];
}

$qtdJogos = 0;
$rodada = 0;
foreach ($grupos as &$grupo) {
	//Variável que vai permitir sair quando o maior pontuador for encontrado
	$indMaiorPontuadorEncontrado = false;
	do {
		$rodada++;
		foreach ($grupo['equipes'] as &$equipe) {
			//Verifica se é o vencedor do grupo. Se for, deixa de analisar
			if ($equipe['pontuacaogrupos'] >= $maxVitorias) {
				continue;
			}

			//Apontando para o grupo, visto que será mudado no próprio array
			$grupoAdvs = &$grupos[$equipe['idgrupo']];

			foreach ($grupoAdvs['equipes'] as &$adv) {
				//Verifica se são a mesma equipe
				if ($adv['nome'] == $equipe['nome']) {
					continue;
				}

				$qtdJogos++;
				$jogo = [
					'numjogo' => $qtdJogos,
					'rodada' => $rodada,
					'nome' => "{$equipe['nome']} vs. {$adv['nome']}"
				];

				$equipe['qtdjogosgrupos']++;
				$adv['qtdjogosgrupos']++;

				list($novoFatorEq1, $novoFatorEq2) = calcFatorDisputa($equipe, $adv);

				if ($novoFatorEq1 > $novoFatorEq2) {
					$equipe['pontuacaogrupos']++;
					$jogo['vencedor'] = $equipe['nome'];
					$jogo['saldopontosvencedor'] = $equipe['pontuacaogrupos'];
					$jogo['saldopontosperdedor'] = $adv['pontuacaogrupos'];
					$jogo['qtdjogosvencedor'] = $equipe['qtdjogosgrupos'];
					$jogo['qtdjogosperdedor'] = $adv['qtdjogosgrupos'];

					if ($equipe['pontuacaogrupos'] >= $maxVitorias) {
						$indMaiorPontuadorEncontrado = true;
						$equipe['indvencedorgrupos'] = true;
						$grupoAdvs['equipesvencedoras'][] = $equipe;
						break 2;
					}
				} else {
					$adv['pontuacaogrupos']++;
					$jogo['vencedor'] = $adv['nome'];
					$jogo['saldopontosvencedor'] = $adv['pontuacaogrupos'];
					$jogo['saldopontosperdedor'] = $equipe['pontuacaogrupos'];
					$jogo['qtdjogosvencedor'] = $adv['qtdjogosgrupos'];
					$jogo['qtdjogosperdedor'] = $equipe['qtdjogosgrupos'];

					if ($adv['pontuacaogrupos'] >= $maxVitorias) {
						$indMaiorPontuadorEncontrado = true;
						$adv['indvencedorgrupos'] = true;
						$grupoAdvs['equipesvencedoras'][] = $adv;
						break 2;
					}
				}
				$jogosgrupos[] = $jogo;
			}
		}
	} while (!$indMaiorPontuadorEncontrado);
}

//Põe os maiores pontuadores em ordem (vencedor e o segundo colocado) no grupo das playoffs
foreach ($grupos as &$grupo) {
	foreach ($grupo['equipes'] as $equipe) {
		$grupo['ordempontuacao'][] = $equipe['pontuacaogrupos'];
	}
	arsort($grupo['ordempontuacao']);

	for ($nval = 15; $nval >= 0; $nval--) {
		foreach ($grupo['equipes'] as $eqp) {
			if ($eqp['pontuacaogrupos'] == $nval) {
				$grupo['equipesvencedoras'][] = $eqp;
				break 2;
			}
		}
	}

	foreach ($grupo['equipesvencedoras'] as $vencedor) {
		$vencedor['indderrotado'] = null;
		$equipesplayoffs[] = $vencedor;
	}
}

function batalhaPlayoffs(&$grupoOrigem, &$grupoDestino)
{
	$qtdMaxVencedores = count($grupoOrigem);
	$maxId = ($qtdMaxVencedores - 1);
	$qtdBatalhas = ($qtdMaxVencedores / 2);
	for ($i = 0; $i < $qtdBatalhas; $i++) {
		do {
			$eq1 = rand(0, $maxId);
		} while ($grupoOrigem[$eq1]['indderrotado'] !== null);
		$grupoOrigem[$eq1]['indderrotado'] = true;

		do {
			$eq2 = rand(0, $maxId);
		} while ($grupoOrigem[$eq2]['indderrotado'] !== null);
		$grupoOrigem[$eq2]['indderrotado'] = true;

		list($novoFatorEq1, $novoFatorEq2) = calcFatorDisputa($grupoOrigem[$eq1], $grupoOrigem[$eq2]);

		if ($novoFatorEq1 > $novoFatorEq2) {
			$grupoOrigem[$eq1]['indderrotado'] = false;
			$grupoOrigem[$eq1]['adversario'] = $grupoOrigem[$eq2];
			$grupoOrigem[$eq2]['adversario'] = $grupoOrigem[$eq1];
			$grupoDestino[] = $grupoOrigem[$eq1];
		} else {
			$grupoOrigem[$eq2]['indderrotado'] = false;
			$grupoOrigem[$eq2]['adversario'] = $grupoOrigem[$eq1];
			$grupoOrigem[$eq1]['adversario'] = $grupoOrigem[$eq2];
			$grupoDestino[] = $grupoOrigem[$eq2];
		}

		$grupoDestino[$i]['indderrotado'] = null;
	}
}

batalhaPlayoffs($equipesplayoffs, $equipesplayoffs_r1);
batalhaPlayoffs($equipesplayoffs_r1, $equipesplayoffs_r2);
batalhaPlayoffs($equipesplayoffs_r2, $equipesplayoffs_r3);
batalhaPlayoffs($equipesplayoffs_r3, $equipesplayoffs_r4);

list($novoFatorEq1, $novoFatorEq2) = calcFatorDisputa($equipesplayoffs_r4[0], $equipesplayoffs_r4[1]);

$vencedor = ($novoFatorEq1 > $novoFatorEq2) ? $equipesplayoffs_r4[0] : $equipesplayoffs_r4[0];
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Simulação de Campeonato</title>
	<style>
		* {
			font-family: arial;
		}

		table {
			border: #000 1px solid;
		}

		table thead th {
			background-color: #000;
			border: 0;
			color: #fff;
		}

		table tbody td {
			border-bottom: #000 1px solid;
			text-align: center;
		}

		table th,
		table td {
			padding: 5px 10px;
		}
	</style>
</head>

<body>
	<h1>Simulação de campeonato</h1>
	<h3>Os grupos e suas equipes por ordem de pontuação</h3>
	<table style="float:left;">
		<thead>
			<tr>
				<th>Grupo</th>
				<th>Equipe</th>
				<th>Poder *</th>
				<th>Pontos</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$eqAnt = '';
			$html = '';
			$qtdGrupos = count($grupos) / 2;
			for ($i = 0; $i < $qtdGrupos; $i++) {
				$qtdEquipes = count($grupos[$i]['equipes']);
				foreach ($grupos[$i]['ordempontuacao'] as $j => $val) {
					$html .= '<tr>';
					if ($eqAnt != $grupos[$i]['nome']) {
						$html .= "<td rowspan=\"5\">{$grupos[$i]['nome']}</td>";
						$eqAnt = $grupos[$i]['nome'];
					}
					$html .= "<td>{$grupos[$i]['equipes'][$j]['nome']}</td>";
					$html .= "<td>{$grupos[$i]['equipes'][$j]['fatorpoder']}</td>";
					$html .= "<td>{$val}</td>";
					$html .= '</tr>';
				}
			}
			echo $html;
			?>
		</tbody>
	</table>
	<table style="float:left;margin-left:10px;">
		<thead>
			<tr>
				<th>Grupo</th>
				<th>Equipe</th>
				<th>Poder *</th>
				<th>Pontos</th>
			</tr>
		</thead>
		<tbody>
			<?php
			$eqAnt = '';
			$html = '';
			for ($i = $qtdGrupos; $i < ($qtdGrupos * 2); $i++) {
				$qtdEquipes = count($grupos[$i]['equipes']);
				foreach ($grupos[$i]['ordempontuacao'] as $j => $val) {
					$html .= '<tr>';
					if ($eqAnt != $grupos[$i]['nome']) {
						$html .= "<td rowspan=\"5\">{$grupos[$i]['nome']}</td>";
						$eqAnt = $grupos[$i]['nome'];
					}
					$html .= "<td>{$grupos[$i]['equipes'][$j]['nome']}</td>";
					$html .= "<td>{$grupos[$i]['equipes'][$j]['fatorpoder']}</td>";
					$html .= "<td>{$val}</td>";
					$html .= '</tr>';
				}
			}
			echo $html;
			?>
		</tbody>
	</table>
	<p style="clear:left;">* O poder é um índice que vai determinar uma equipe forte e outra fraca para permitir que não seja sujeita a sorte durante o andar do campeonato. Isso significa que, se a equipe for realmente forte, ela se manterá forte até a final.</p>
	<h3>Os jogos da fase de grupos</h3>
	<table style="float:left;margin-left:10px;margin-bottom:10px;">
		<thead>
			<tr>
				<th># Jogo</th>
				<th># Rodada</th>
				<th>Times</th>
				<th>Equipe vencedora</th>
				<th>Pontos do vencedor</th>
				<th>Pontos do perdedor</th>
				<th>Jogos do vencedor</th>
				<th>Jogos do perdedor</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($jogosgrupos as $jogo) : ?>
				<tr>
					<td><?= $jogo['numjogo'] ?></td>
					<td><?= $jogo['rodada'] ?></td>
					<td><?= $jogo['nome'] ?></td>
					<td><?= $jogo['vencedor'] ?></td>
					<td><?= $jogo['saldopontosvencedor'] ?></td>
					<td><?= $jogo['saldopontosperdedor'] ?></td>
					<td><?= $jogo['qtdjogosvencedor'] ?></td>
					<td><?= $jogo['qtdjogosperdedor'] ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<h3 style="clear:left;">Os jogos da fase de Playoffs (32 adversários, 16 jogos)</h3>
	<table>
		<thead>
			<tr>
				<th>Time</th>
				<th>Resultado</th>
				<th>Adversário</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($equipesplayoffs as $jogo) : if ($jogo['indderrotado']) continue; ?>
				<tr>
					<td><?= $jogo['nome'] ?></td>
					<td style="background-color:#b9ebb1">venceu</td>
					<td><?= $jogo['adversario']['nome'] ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<h3 style="clear:left;">Os jogos da fase de Playoffs (16 adversários, 8 jogos)</h3>
	<table>
		<thead>
			<tr>
				<th>Time</th>
				<th>Resultado</th>
				<th>Adversário</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($equipesplayoffs_r1 as $jogo) : if ($jogo['indderrotado']) continue; ?>
				<tr>
					<td><?= $jogo['nome'] ?></td>
					<td style="background-color:#b9ebb1">venceu</td>
					<td><?= $jogo['adversario']['nome'] ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<h3 style="clear:left;">Os jogos da fase de Playoffs (8 adversários, 4 jogos)</h3>
	<table>
		<thead>
			<tr>
				<th>Time</th>
				<th>Resultado</th>
				<th>Adversário</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($equipesplayoffs_r2 as $jogo) : if ($jogo['indderrotado']) continue; ?>
				<tr>
					<td><?= $jogo['nome'] ?></td>
					<td style="background-color:#b9ebb1">venceu</td>
					<td><?= $jogo['adversario']['nome'] ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<h3 style="clear:left;">Os jogos da fase de Playoffs (4 adversários, 2 jogos) - Semifinal</h3>
	<table>
		<thead>
			<tr>
				<th>Time</th>
				<th>Resultado</th>
				<th>Adversário</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($equipesplayoffs_r3 as $jogo) : if ($jogo['indderrotado']) continue; ?>
				<tr>
					<td><?= $jogo['nome'] ?></td>
					<td style="background-color:#b9ebb1">venceu</td>
					<td><?= $jogo['adversario']['nome'] ?></td>
				</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<h3>O grande vencedor</h3>
	<p style="text-align:center;background-color:#ffff84;font-size:2em;font-weight:bold;"><?= $vencedor['nome'] ?></p>
</body>

</html>